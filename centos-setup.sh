#!/bin/bash
#
# centos-setup.sh
#
# (c) Charly RAUX 2020

VERSION="el7"

# Répertoire actuel
CWD=$(pwd)

# Utilisateurs définis
USERS="$(ls -A /home)"

# Admin user
ADMIN=$(getent passwd 1000 | cut -d : -f 1)

# Retirer ces paquets
CRUFT=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/useless-packages.txt)

# Installez ces paquets
EXTRA=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/extra-packages.txt)

# Utilisateurs définis
USERS="$(ls -A /home)"

# Miroirs
ELREPO="https://elrepo.org/linux/elrepo/${VERSION}/x86_64/RPMS"
CISOFY="https://packages.cisofy.com"

# Log
LOG="/tmp/$(nom de base "${0}" .sh).log"
echo > ${LOG}

usage() {
  faire écho à "Usage : ${0} OPTION"
  echo 'CentOS 7.x post-install configuration for servers'.
  echo "Options :
  echo ' -1, --shell Configurer le shell : Bash, Vim, console, etc.
  echo ' -2, --repos Configurer les dépôts officiels et tiers".
  echo ' -3, --extra Installer le système de base amélioré.
  echo ' -4, --prune Supprimer les paquets inutiles.
  echo ' -5, --logs Permettre à l'utilisateur administrateur d'accéder aux journaux du système.
  echo ' -6, --ipv4 Désactiver IPv6 et reconfigurer les services de base.' echo ' -6, --ipv4 Désactiver IPv6 et reconfigurer les services de base.'
  echo ' -7, --sudo Configurer le mot de passe persistant pour sudo.
  echo ' -8, --setup Effectuer tout ce qui précède en une seule fois.
  echo ' -9, --strip Revenir au système de base amélioré.
  echo ' -h, --help Afficher ce message.
  echo ' -h, --help Afficher ce message.' echo ' -h, --help Afficher ce message.' echo ' -h, --help Les logs sont écrits dans ${LOG}.
}

configure_shell() {
  # Installer des invites de commande personnalisées et une poignée d'alias.
  echo 'Configurer le shell Bash pour root'.
  cat ${CWD}/${VERSION}/bash/bashrc-root > /root/.bashrc
  echo 'Configurer le shell Bash pour les utilisateurs'.
  cat ${CWD}/${VERSION}/bash/bashrc-users > /etc/skel/.bashrc
  # Les utilisateurs existants pourraient vouloir l'utiliser.
  if [ ! -z "${USERS}" ]
  puis
    pour l'UTILISATEUR en ${USERS}
    faire
      cat ${CWD}/${VERSION}/bash/bashrc-users > /home/${USER}/.bashrc
      chown ${USER}:${USER} /home/${USER}/.bashrc
    fait
  fi
  # Ajouter une poignée d'options astucieuses à l'échelle du système pour Vim.
  echo 'Configurer Vim.
  cat ${CWD}/${VERSION}/vim/vimrc > /etc/vimrc
  # Définir l'anglais comme langue principale du système.
  echo 'Configurer la langue du système'.
  localectl set-locale LANG=fr_US.UTF8
  # Régler la résolution de la console
  si [ -f /boot/grub2/grub.cfg ]
  puis
    echo 'Configurer la résolution de la console'.
    sed -i -e 's/rhgb quiet/nomodeset quiet vga=791/g' /etc/default/grub
    grub2-mkconfig -o /boot/grub2/grub.cfg >> ${LOG} 2>&1
  fi
}

configure_repos() {
  # Activer les [bases], [mises à jour] et [extra] repos avec une priorité de 1.
  echo 'Configurer les dépôts officiels de paquets'.
  cat ${CWD}/${VERSION}/yum/CentOS-Base.repo > /etc/yum.repos.d/CentOS-Base.repo
  sed -i -e 's/installonly_limit=5/installonly_limit=2/g' /etc/yum.conf
  # Permettre la mise en pension [cr] avec une priorité de 1.
  echo 'Configurer le dépôt de paquets CR'.
  cat ${CWD}/${VERSION}/yum/CentOS-CR.repo > /etc/yum.repos.d/CentOS-CR.repo
  # Activer les repos [sclo] avec une priorité de 1.
  echo 'Configurer les dépôts de paquets SCLo'.
  if ! rpm -q centos-release-scl > /dev/null 2>&1
  puis
    yum -y install centos-release-scl >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl-rh.repo > /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl.repo > /etc/yum.repos.d/CentOS-SCLo-scl.repo
  # Activer le RPM Delta.
  if ! rpm -q deltarpm > /dev/null 2>&1
  puis
    écho "Activation du Delta RPM".
    yum -y install deltarpm >> ${LOG} 2>&1
  fi
  # Mise à jour initiale
  echo "Effectuer une mise à jour initiale".
  echo "Cela peut prendre un moment...
  yum -y mise à jour >> ${LOG} 2>&1
  # Installer le plugin Yum-Priorities
  if ! rpm -q yum-plugin-priorities > /dev/null 2>&1
  puis
    echo 'Installer le plugin Yum-Priorities'.
    yum -y install yum-plugin-priorities >> ${LOG} 2>&1
  fi
  # Permettre la mise en pension [epel] avec une priorité de 10.
  echo 'Configurer le dépôt de paquets EPEL'. 
  if ! rpm -q epel-release > /dev/null 2>&1
  puis
    yum -y install epel-release >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/epel.repo > /etc/yum.repos.d/epel.repo
  cat ${CWD}/${VERSION}/yum/epel-testing.repo > /etc/yum.repos.d/epel-testing.repo
  # Configurer les repos [elrepo] et [elrepo-kernel] sans les activer.
  echo 'Configurer les dépôts de paquets ELRepo'.
  if ! rpm -q elrepo-release > /dev/null 2>&1
  puis
    miam -y localinstall \
    ${ELREPO}/elrepo-release-7.0-4.${VERSION}.elrepo.noarch.rpm >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/elrepo.repo > /etc/yum.repos.d/elrepo.repo
  # Activer la prise en pension [lynis] avec une priorité de 5.
  echo 'Configurer le dépôt de paquets Lynis'.
  if [ ! -f /etc/yum.repos.d/lynis.repo ]
  puis
    rpm --import ${CISOFY}/keys/cisofy-software-rpms-public.key >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/lynis.repo > /etc/yum.repos.d/lynis.repo
}

install_extras() {
  echo 'Récupération des paquets manquants du groupe de paquets de base'. 
  yum group mark remove "Core" >> ${LOG} 2>&1
  yum -y group install "Core" >> ${LOG} 2>&1
  echo 'Groupe de paquets de base installé sur le système'.
  echo 'Installation du groupe de paquets de base
